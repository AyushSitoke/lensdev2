package com.lti.lens.core.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SampleTest {

	@Test
	public void simpleTestPass() {
		int a = 2;
		int b = 1;
		assertTrue(a+b==3);
	}

	@Test
	public void simpleTestFail() {
		int a = 2;
		int b = 1;
		assertTrue(a+b==4);
	}
}
