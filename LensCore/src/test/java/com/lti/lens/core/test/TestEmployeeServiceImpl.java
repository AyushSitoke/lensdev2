package com.lti.lens.core.test;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.lti.lens.core.dao.EmployeeDao;
import com.lti.lens.core.model.Employee;
import com.lti.lens.core.service.EmployeeService;
import com.lti.lens.core.service.EmployeeServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TestEmployeeServiceImpl {
	ArgumentCaptor<Employee> clientArgumentCaptured = ArgumentCaptor.forClass(Employee.class);

	@Mock
	private EmployeeDao employeeDao;
	
	@InjectMocks
	private EmployeeService employeeService = new EmployeeServiceImpl();
	
	@Before
	public void initializeMockito() {
		MockitoAnnotations.initMocks(this);
	}
		
	@Test
	public void testSaveEmployee() {
		Employee emp = new Employee(10, 1000);
		stub(employeeDao.saveEmployee(any(Employee.class))).toReturn(10);
		assertTrue(10==employeeService.saveEmployee(emp));
	}

	@Test
	public void testGetEmployees() {
		Employee emp = new Employee(101, 1000);
		stub(employeeDao.getById(anyInt())).toReturn(emp);
		assertTrue(emp.equals(employeeService.getById(10)) );
	}

	@Test
	public void testGetTotalSal() {
		List<Employee> employees = Arrays.asList(
				new Employee(101, 1000), new Employee(102, 2000));
		
		stub(employeeDao.getEmployees()).toReturn(employees);
		assertTrue(4000.0==employeeService.getTotalSal());
	}

	@Test
	public void testGetById() {
		Mockito.when(employeeDao.getById(1)).thenReturn(new Employee(1,"Some Name", 1000));
		Mockito.when(employeeDao.getById(2)).thenReturn(new Employee(2,"Second Name", 2000));

		Employee e = employeeService.getById(2);
		assertEquals("Second Name", e.getName());
		assertTrue(2000.0==e.getSalary());
		
		Mockito.verify(employeeDao).getById(2);
	}
}
