package com.lti.lensCore;

import java.util.List;

import org.springframework.beans.factory.BeanFactory;  
import org.springframework.beans.factory.xml.XmlBeanFactory;  
import org.springframework.core.io.ClassPathResource;  
import org.springframework.core.io.Resource;

import com.lti.lens.core.dao.EmployeeDao;
import com.lti.lens.core.model.Employee;  
  
public class TestInsert {  
	public static void main(String[] args) {  
	      
	    Resource r=new ClassPathResource("/hibernate/applicationContext.xml");  
	    BeanFactory factory=new XmlBeanFactory(r);  
	      
	    EmployeeDao dao=(EmployeeDao)factory.getBean("employeeDaoImpl");
	      
	    Employee e=new Employee(120, "Arun", 501);  

	    dao.saveEmployee(e);  
	    
	    e = dao.getById(114);
	    dao.deleteEmployee(e);
	    
	    List<Employee> list = dao.getEmployees();
	    for(Employee e1 : list) {
	    	System.out.println(e1.getId()+"\t"+e1.getName()+"\t"+e1.getSalary());
	    }
	    
	}  
}  