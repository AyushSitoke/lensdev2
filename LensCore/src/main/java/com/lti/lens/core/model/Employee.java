package com.lti.lens.core.model;

public class Employee {
    private int id;  
    private String name;  
    private double salary;
      
	public Employee(int id, String name, double salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	
	public Employee(int id, double sal) {
		this(id,"Emp-"+id,sal);
	}
	
	public Employee() {
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}  
	
	public boolean equals(Object o) {
		if(o instanceof Employee) {
			Employee oE = (Employee) o;
			return (this.id==oE.id
					&& this.getName().equals(oE.getName())
					&& this.getSalary()==oE.getSalary());
		}
		return false;
	}
      
}
