package com.lti.lens.core.service;

import org.springframework.orm.hibernate3.HibernateTemplate;

import com.lti.lens.core.dao.EmployeeDao;
import com.lti.lens.core.model.Employee;

import java.util.*;

public class EmployeeServiceImpl implements EmployeeService {

	EmployeeDao employeeDao;
	
	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	public int saveEmployee(Employee e) {
		return employeeDao.saveEmployee(e);
	}

	public void updateEmployee(Employee e) {
		employeeDao.updateEmployee(e);
	}

	public void deleteEmployee(Employee e) {
		employeeDao.deleteEmployee(e);
	}

	public Employee getById(int id) {
		Employee e = (Employee) employeeDao.getById(id);
		return e;
	}

	public List<Employee> getEmployees() {
		List<Employee> list = new ArrayList<Employee>();
		list = employeeDao.getEmployees();
		return list;
	}
	
	public double mySaveUpdateDelete(Employee e) {
		employeeDao.saveEmployee(e);
		
		e.setId(999);
		employeeDao.updateEmployee(e);
		
		return e.getId();
		
	}
	
	public double getTotalSal() {
		List<Employee> existingEmps = employeeDao.getEmployees();		
		double total = 0;
		for(Employee e : existingEmps) {
			total += e.getSalary();
		}
		return total;
	}
	

}