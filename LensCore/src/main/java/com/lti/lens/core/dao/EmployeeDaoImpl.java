package com.lti.lens.core.dao;

import org.springframework.orm.hibernate3.HibernateTemplate;

import com.lti.lens.core.model.Employee;

import java.util.*;

public class EmployeeDaoImpl implements EmployeeDao {
	HibernateTemplate template;

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}

	public int saveEmployee(Employee e) {
		return (Integer) template.save(e);
	}

	public void updateEmployee(Employee e) {
		template.update(e);
	}

	public void deleteEmployee(Employee e) {
		template.delete(e);
	}

	public Employee getById(int id) {
		Employee e = (Employee) template.get(Employee.class, id);
		return e;
	}

	public List<Employee> getEmployees() {
		List<Employee> list = new ArrayList<Employee>();
		list = template.loadAll(Employee.class);
		return list;
	}
}